<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pruebasdb extends CI_Controller {

    public function clientes_beta(){
        //cargo la database
        $this->load->database();

        //ejecuto el query de la database
        $query =$this->db->query('SELECT id, nombre, correo, telefono1 FROM clientes limit 10');

        // foreach($query->result() as $row){
        //     echo $row->id;
        //     echo $row->nombre;
        //     echo $row->correo;
        // }

        // echo 'Total Registros: '.$query->num_rows();

        //recorre toda la tabla para hacer el JSON
        $respuesta = array(
            'err' => FALSE,
            'mensaje' => 'Registros cargados correctamente',
            'total_registros' =>$query->num_rows(),
            'clientes'=>$query->result()

        );
        //imprimo el JSON
        echo json_encode($respuesta);
    }


    public function cliente($id){
        //cargo la datbase
        $this->load->database();

        //ejecuto el query
        $query =$this->db->query('SELECT * FROM clientes where id = ' .$id);
        //regresa una fila
        $fila = $query->row();

        //validar
        if(isset($fila)){
            //Fila existe
            $respuesta = array(
                'err' => FALSE,
                'mensaje' => 'Registro cargado correctamente',
                'total_registros' =>1,
                'clientes'=>$fila    
            );
        }else{
            //fila no existe
            $respuesta = array(
                'err' => TRUE,
                'mensaje' => 'El registro con el id '.$id.', no existe',
                'total_registros' =>0,
                'cliente'=>null    
            );

        }
        //imprimo el json
        echo json_encode($fila);
    }


}