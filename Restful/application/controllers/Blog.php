<?php
//va en todos los controladores
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller{
    public function index(){
        echo "Hola mundo";
    }
    //para que el metodo reciba un parametro de la url ($parametro)
    public function Comentarios($id){
        //validando que no sean letras
        if(!is_numeric($id)){
            $respuesta = array('err' => true , 'mensaje'=>'el id tiene que ser numerico' );

            echo json_encode($respuesta);

            return;
        };
       
        //Arreglo de comentarios asumiendo q esta es la BD
        $comentarios = array(
            array('id' => 1, 'mensaje' => 'lorem ipsum1'),
            array('id' => 2, 'mensaje' => 'lorem ipsum2'),
            array('id' => 3, 'mensaje' => 'lorem ipsum3')
        );

        //validondo cuando es mayor el numero id que se ingresa en la url
        if($id >= count ($comentarios) OR $id < 0 ){
            $respuesta = array('err' => true, 'mensaje'=>'El id no existe');

            echo json_encode($respuesta);

            return;
        }
        //lo regrsamos con json_encode, para poder mostrar el id se coloca como [$parametro]
        echo json_encode($comentarios[$id]);
    }
}