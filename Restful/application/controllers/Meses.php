<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//creo la clase
class Meses extends CI_Controller{
    //creo la funcion
    public function mes($mes){

        //cargar helper
        $this->load->helper('utilidades');
        //imprimir el contenido del helper --> meses
        echo json_encode(obtener_mes($mes));

        // $mes -=1;
    
        // $meses = array(
        //     'enero', 
        //     'febrero', 
        //     'marzo', 
        //     'abril', 
        //     'mayo', 
        //     'junio', 
        //     'julio', 
        //     'agosto', 
        //     'septiembre', 
        //     'octubre', 
        //     'noviembre', 
        //     'diciembre'
        // );
        // echo json_encode($meses[$mes]);
        
    }
}