
<?php
//los controladores no llevan cierre de etiqueta php 

//sintaxis NombreControlador.php
// la primera mayuscula

//siempre defino estas lineas de codigo
defined('BASEPATH') OR exit('No direct script access allowed');
// la clase se tiene que llamar y escribir como el NombreControlador
class Clientes extends CI_Controller{

    public function index(){
        //cargo helper
        $this->load->helper('utilidades');
        //asumiendo que esta es mi BD
        $data = array(
            //esta seria la peticion
            'nombre'    => 'Cristhian Barahona',
            'contacto'  => 'Maybelline Sauceda',
            'direccion' => 'Lomas de san jose'
        );
        //convierto a mayusculas
        // $data['nombre'] = strtoupper($data['nombre']);
        // $data['contacto'] = strtoupper($data['contacto']);
        //imprimo la data

        $campos_capitalizar = array('nombre', 'contacto');

        $data = capitalizar_arreglo($data, $campos_capitalizar);

        echo json_encode($data);
    }

    //cargando el modelo
    public function cliente($id){
        //cargo el modelo
        $this->load->model('Cliente_model');
        $cliente = $this->Cliente_model->get_cliente($id);

        echo json_encode($cliente);
    }




}
